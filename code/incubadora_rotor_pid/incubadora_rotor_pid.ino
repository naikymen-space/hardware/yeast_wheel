/* Awesome thing for yeast liquid cultures at regulated rotating speed and temperature. */

/********************************************************
* PID speed control
* https://www.instructables.com/id/Make-your-own-Lathe-from-other-peoples-rubbish/
* universal_ac_motor_pid_control_ino.c.ino
********************************************************/
#include <PID_v1.h>

int motor = 9;         // the PWM pin the MOTOR is attached to
int heater = 10;       // the PWM pin the HEATER is attached to

// Setpoint heaterPID (degrees celsius)
double Setpoint = 28;
// Specify the links and initial tuning parameters for the heaterPID
double Kp=400, Ki=2, Kd=10;

// Setpoint motorPID (real RPM)
double Setpoint2 = 60;
// Specify the links and initial tuning parameters for the motorPID
double Kp2=0.5, Ki2=1.5, Kd2=0;

// Thermometer data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2

// Tachometer (hall effect sensor) stuff
#define TACHO 3            // tacho signals input pin (either 2 or 3 in the Arduino Uno)
#define TACHOPULSES 1      // number of pulses per revolution

/********************************************************
 * Heater PID
*********************************************************/

//Define Variables we'll be connecting to
double Input, Output;
// Initialize PID
PID heaterPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);


/********************************************************
* OneWire temperature measurement
*********************************************************/

// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);


/********************************************************
 * Motor PID
*********************************************************/
int speed;  // variable, MOTOR speed signal

// Define Variables we'll be connecting to
double Input2, Output2;
// Initialize PID
PID motorPID(&Input2, &Output2, &Setpoint2, Kp2, Ki2, Kd2, DIRECT);

#include <avr/io.h>
#include <avr/interrupt.h>

// RPM counting variables
float RPM;                          // real rpm variable
unsigned int count;                 // tacho pulses count variable
unsigned int lastcount = 0;         // additional tacho pulses count variable
unsigned long lastcounttime = 0;
//unsigned long lastflash;
unsigned long lastpiddelay = 0;
unsigned long previousMillis = 0;
const int rpmReportInterval = 3000;  // rpm report interval, in ms

// RPM counting routine
void tacho() {
  count++;
  Serial.print("Tacho fired, count: ");
  Serial.println(count);
}

// RPM averaging
// https://www.arduino.cc/en/Tutorial/BuiltInExamples/Smoothing
const int numReadings = 5;
float readings[numReadings];    // the readings from the analog input
int readIndex = 0;              // the index of the current reading
float total = 0;                // the running total
float averageRPM = 0;           // the average


/********************************************************
 * SETUP
*********************************************************/

void setup() {
  // start serial port (plotting setup)
  Serial.begin(9600);
  Serial.print("Input_temperature");
  Serial.print(" ");
  Serial.print("Output");
  Serial.print(" ");
  Serial.print("Input2_RPM");
  Serial.print(" ");
  Serial.println("Output2");

  // Set-up interrupt pins
  // https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  pinMode(TACHO, INPUT);              // set the tacho pulses detect pin // ¿pinMode(interruptPin, INPUT_PULLUP);?
  // set up tacho sensor interrupt IRQ1 on pin3
  attachInterrupt(digitalPinToInterrupt(TACHO), tacho, FALLING);

  // Initialize RPM averaging
  // set all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }

  // Declare motor and heater pins to be outputs:
  pinMode(motor, OUTPUT);
  pinMode(heater, OUTPUT);

  // Start up the One-wire library
  sensors.begin();

  // Initialize the variables heaterPID is linked to:
  sensors.requestTemperatures();
  Input = sensors.getTempCByIndex(0);

  // Initialize the variables motorPID is linked to:
  Input2 = 0;

  // turn the PID on
  heaterPID.SetMode(AUTOMATIC);
  motorPID.SetMode(AUTOMATIC);
}

/********************************************************
 * LOOP
 ********************************************************/

void loop() {
  // heaterPID
  sensors.requestTemperatures();
  Input = sensors.getTempCByIndex(0);
  heaterPID.Compute();
  analogWrite(heater, Output);

  // motorPID
  unsigned long counttime = millis();
  // Refresh after rpmReportInterval/1000 seconds of counting tacho pulses:
  if (counttime - lastcounttime >= rpmReportInterval) {
      // record the counts in "lastcount"
      lastcount = count;
      // and reset count to zero
      count = 0;

      // Compute RPM:
      float prerpm = 60.0 * 1000.0 * float(lastcount) / float(rpmReportInterval);
      RPM = prerpm / TACHOPULSES;

      // Averaging RPM
      // subtract the previous reading fron total:
      total = total - readings[readIndex];
      // read from the sensor, and replace the reading we just subtracted with the new one:
      readings[readIndex] = RPM;
      // now add the new reading to the total:
      total = total + readings[readIndex];
      // and advance to the next position in the array:
      readIndex = readIndex + 1;

      // if we're at the end of the array...
      if (readIndex >= numReadings) {
          // ...wrap around to the beginning:
          readIndex = 0;
      }

      // calculate the average:
      averageRPM = total / numReadings;
      //Serial.print("RPM: ");
      //Serial.print(RPM);
      //Serial.print("  averageRPM: ");
      //Serial.println(averageRPM);

      // Compute motorPID;
      Input2 = averageRPM;
      motorPID.Compute();
      analogWrite(motor, Output2);

      // reset last count time to the present
      lastcounttime = millis();
  }

  Serial.print(Input);

  Serial.print(" ");
  Serial.print(Output);

  Serial.print(" ");
  Serial.print(Input2);

  Serial.print(" ");
  Serial.println(Output2);
}
